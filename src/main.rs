use flate2::read::GzDecoder;
use std::{os::unix::process::CommandExt, process::Command};
use tar::Archive;

#[derive(Debug, Clone)]
struct Package {
    name: String,
    basename: String,
}

fn main() {
    let login: *mut libc::passwd = unsafe { libc::getpwnam(libc::getlogin()) };
    if login.is_null() {
        println!("Fatal: could not read current user.");
        return;
    }
    let userid: u32 = unsafe { *login }.pw_uid;
    let groupid: u32 = unsafe { *login }.pw_gid;
    let username: *mut i8 = unsafe { *login }.pw_name;

    let mut groupids: Vec<u32> = vec![0; 1024];
    let mut groupn: i32 = groupids.len() as i32;
    let res: i32 =
        unsafe { libc::getgrouplist(username, groupid, (*groupids).as_mut_ptr(), &mut groupn) };
    if res < 0 {
        println!("failed to get user groups!");
        return;
    }
    groupids.truncate(groupn as usize);

    let mut root_groupids: Vec<u32> = vec![0; 1024];
    let mut root_groupn: i32 = root_groupids.len() as i32;
    let res: i32 = unsafe {
        libc::getgrouplist(
            username,
            groupid,
            (*root_groupids).as_mut_ptr(),
            &mut root_groupn,
        )
    };
    if res < 0 {
        println!("failed to get user groups!");
        return;
    }
    root_groupids.truncate(root_groupn as usize);

    unsafe { libc::seteuid(userid) };
    let base_userid: u32 = unsafe { libc::getuid() };
    if base_userid != 0 {
        elevate();
        return;
    }
    let home = std::env::var("HOME");
    if home.is_err() {
        println!("Fatal: do not run this program using sudo without passing on the neccesary env variables.");
    }

    let tmp_user_dir = home.unwrap();
    let user_directory: &std::path::Path = std::path::Path::new(tmp_user_dir.as_str());
    let mut isdep: bool = false;
    for arg in std::env::args().skip(1) {
        match arg.as_str() {
            "--asdep" => isdep = true,
            "update" => update(user_directory, userid, groupids.clone(), false),
            _ => install_package_with_deps(user_directory, arg, userid, groupids.clone(), isdep),
        }
    }
}

fn update(user_directory: &std::path::Path, userid: u32, groupids: Vec<u32>, asdep: bool) {
    let update_out: String = String::from_utf8(
        std::process::Command::new("auracle")
            .arg("outdated")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    let update_names: Vec<&str> = update_out
        .split('\n')
        .map(|line: &str| {
            *line
                .split_whitespace()
                .take(1)
                .collect::<Vec<&str>>()
                .first()
                .unwrap_or(&"")
        })
        .filter(|name: &&str| !name.is_empty())
        .collect();
    if update_names.is_empty() {
        println!("no package need updating");
    }
    for name in update_names {
        println!("updating package: {}", name);
        install_package_with_deps(
            user_directory,
            name.to_string(),
            userid,
            groupids.clone(),
            asdep,
        );
    }
}

fn install_package_with_deps(
    user_directory: &std::path::Path,
    pkgname: String,
    userid: u32,
    groupids: Vec<u32>,
    asdep: bool,
) {
    let dependency_out: String = String::from_utf8(
        std::process::Command::new("auracle")
            .arg("buildorder")
            .arg(pkgname.clone())
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    let package: Package = dependency_out
        .split('\n')
        .map(|line: &str| line.split_whitespace().take(3).collect::<Vec<&str>>())
        .filter(|line: &Vec<&str>| !line.is_empty() && line[0] == "TARGETAUR")
        .map(|line: Vec<&str>| Package{name: line[1].to_string(), basename: line[2].to_string()})
        .collect::<Vec<Package>>()[0].clone();

    let aurdeps_names: Vec<Package> = dependency_out
        .split('\n')
        .map(|line: &str| line.split_whitespace().take(3).collect::<Vec<&str>>())
        .filter(|line: &Vec<&str>| !line.is_empty() && line[0] == "AUR")
        .map(|line: Vec<&str>| Package{name: line[1].to_string(), basename: line[2].to_string()})
        .collect();

    for dep in aurdeps_names {
        println!("installing dependency: {}", dep.name);
        install_package(
            user_directory,
            dep,
            userid,
            std::sync::Arc::new(groupids.clone()),
            true,
        );
    }

    install_package(
        user_directory,
        package,
        userid,
        std::sync::Arc::new(groupids),
        asdep,
    );
}

fn install_package(
    user_directory: &std::path::Path,
    package: Package,
    userid: u32,
    groupids: std::sync::Arc<Vec<u32>>,
    asdep: bool,
) {
    let applicationspath: std::path::PathBuf =
        std::path::PathBuf::from(user_directory).join(std::path::Path::new("applications"));
    let applicationpath: std::path::PathBuf = applicationspath.join(std::path::Path::new(&package.basename));

    std::fs::create_dir_all(&applicationspath).unwrap();
    std::env::set_current_dir(&applicationspath).unwrap();

    if download_package(package.basename).is_none() {
        println!("Fatal: could not download package");
    }

    std::env::set_current_dir(&applicationpath).unwrap();

    let mut ret: i32 = unsafe { libc::seteuid(0) };
    if ret != 0 {
        println!("Fatal: could not set effective userid");
        return;
    }

    let goup_count: usize = groupids.len();
    let groupids_cloned = std::sync::Arc::clone(&groupids);
    unsafe {
        std::process::Command::new("makepkg").pre_exec(move || {
            libc::setgroups(goup_count, (*groupids_cloned).as_ptr());
            libc::setuid(userid);
            Ok(())
        })
    }
    .arg("-Ascf")
    .arg("--noconfirm")
    .stdout(std::process::Stdio::inherit())
    .stderr(std::process::Stdio::inherit())
    .output()
    .unwrap();

    let groupids_cloned = std::sync::Arc::clone(&groupids);
    let packages = std::string::String::from_utf8(
        unsafe {
            std::process::Command::new("makepkg").pre_exec(move || {
                libc::setgroups(goup_count, (*groupids_cloned).as_ptr());
                libc::setuid(userid);
                Ok(())
            })
        }
        .arg("--packagelist")
        .output()
        .unwrap()
        .stdout,
    )
    .unwrap();

    ret = unsafe { libc::seteuid(userid) };
    if ret != 0 {
        println!("Fatal: could not set effective userid");
        return;
    }

    let mut filenames: Vec<&str> = packages.split('\n').collect();
    filenames.pop();

    ret = unsafe { libc::seteuid(0) };
    if ret != 0 {
        println!("Fatal: could not set effective userid");
        return;
    }
    std::process::Command::new("pacman")
        .arg("-U")
        .args(filenames)
        .arg("--noconfirm")
        .arg(if asdep { "--asdeps" } else { "--asexplicit" })
        .stdout(std::process::Stdio::inherit())
        .stderr(std::process::Stdio::inherit())
        .output()
        .unwrap();

    ret = unsafe { libc::seteuid(userid) };
    if ret != 0 {
        println!("Fatal: could not set effective userid");
    }
}

fn download_package(name: String) -> Option<()> {
    let request: ureq::Response = ureq::get(
        format!(
            "https://aur.archlinux.org/cgit/aur.git/snapshot/{}.tar.gz",
            name
        )
        .as_str(),
    )
    .send_string("");
    if request.ok() {
        let tar = GzDecoder::new(request.into_reader());
        let mut archive = Archive::new(tar);
        archive.unpack(".").unwrap();
        Some(())
    } else {
        println!("Fatal: could not download package:{}", request.status());
        None
    }
}

fn elevate() -> Option<()> {
    let mut args: Vec<String> = std::env::args().collect();
    if let Some(absolute_path) = std::env::current_exe()
        .ok()
        .and_then(|p| p.to_str().map(|p| p.to_string()))
    {
        args[0] = absolute_path;
    }
    let mut command: Command = Command::new("/usr/bin/sudo");

    //propogate variables
    if let Ok(trace) = std::env::var("RUST_BACKTRACE") {
        command.arg(format!("RUST_BACKTRACE={}", trace));
    }
    if let Ok(trace) = std::env::var("SSH_AUTH_SOCK") {
        command.arg(format!("SSH_AUTH_SOCK={}", trace));
    }
    if let Ok(trace) = std::env::var("HOME") {
        command.arg(format!("HOME={}", trace));
    }
    let mut child = command.args(args).spawn().expect("failed start proccess");
    let err = child.wait().expect("failed to elevate priviliges");
    if !err.success() {
        std::process::exit(err.code().unwrap_or(1));
    } else {
        std::process::exit(0);
    }
}
